# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

extends Timer

@onready var start_button : Button = $StartButton

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var error: Error = self.connect("timeout", Callable(self, "_timeout"))
	if error != OK:
		printerr("Could not connect Timer to _timeout function")

func _timeout() -> void:
	start_button.show()
	start_button.grab_focus()
