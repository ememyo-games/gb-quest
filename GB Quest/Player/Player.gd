# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

class_name Player
extends CharacterBody2D

const SPEED: int = 60 # Should match FPS for smooth movement.

enum Facing {
	UP,
	RIGHT,
	DOWN,
	LEFT
}

var last_pressed_time_up: int = 0
var last_pressed_time_left: int = 0
var last_pressed_time_right: int = 0
var last_pressed_time_down: int = 0

@onready var animationPlayer: AnimationPlayer = $AnimationPlayer
@onready var interactionArea: Area2D = $InteractionArea
@onready var interactionShape: CollisionShape2D = $InteractionArea/CollisionShape2D
@onready var black_fade: BlackFade = $Black
@onready var dialogue: Dialogue = $Camera2D/CanvasLayer/Dialogue
@onready var main_menu: MainMenu = $Camera2D/CanvasLayer/MainMenu

var facing: Facing = Facing.DOWN
var movement_vector: Vector2 = Vector2.ZERO
var collision: KinematicCollision2D
var input: Vector2

signal player_ready

func update_interacton_position() -> void:
	match facing:
		Facing.UP:
			interactionShape.position = Vector2(0, -6)
		Facing.RIGHT:
			interactionShape.position = Vector2(9, 3)
		Facing.DOWN:
			interactionShape.position = Vector2(0, 12)
		Facing.LEFT:
			interactionShape.position = Vector2(-9, 3)


func _ready() -> void:
	input = Vector2.ZERO
	Global.player = self
	if PlayerData.pre_battle_position != Vector2.ZERO:
		position = PlayerData.pre_battle_position


func do_fade_in() -> void:
	get_tree().paused = true
	await black_fade.fade_out()
	get_tree().paused = false


func _physics_process(delta: float) -> void:
	movement_vector = update_movement_input()
	if (movement_vector != Vector2.ZERO):
		animation_selection(movement_vector)
		update_interacton_position()
		velocity = movement_vector
	else:
		idle_animation()
		velocity = Vector2.ZERO

	collision = move_and_collide(velocity * SPEED * delta)
	if collision:
		var collider: Node2D = collision.get_collider()
		if collider.is_in_group("Enemy"):
			var enemy_collider: Enemy = collider
			get_tree().paused = true
			await black_fade.fade_in()
			get_tree().paused = false
			enemy_collider.start_battle()


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("ui_accept"):
		interact()


func interact() -> void:
	var overlaps: Array[Node2D] = interactionArea.get_overlapping_bodies()
	if overlaps.size() > 0:
		for overlap: Node2D in overlaps:
			print("Interact with  ", overlap.name)
			if overlap is Item:
				print("It's an item")
				var overlap_item: Item = overlap
				overlap_item.interact(self)
				get_tree().paused = true
				dialogue.show_message("You picked up " + overlap_item.get_determinative())
			else:
				print("It's not an item")


func fade_out() -> void:
	get_tree().paused = true
	await black_fade.fade_in()


func fade_in() -> void:
	await black_fade.fade_out()
	get_tree().paused = false


func animation_selection(animation_vector: Vector2) -> void:
	if animation_vector.x > 0:
		animationPlayer.play("RunRight")
		facing = Facing.RIGHT
	elif animation_vector.x < 0:
		animationPlayer.play("RunLeft")
		facing = Facing.LEFT
	elif animation_vector.y < 0:
		animationPlayer.play("RunUp")
		facing = Facing.UP
	elif animation_vector.y > 0:
		animationPlayer.play("RunDown")
		facing = Facing.DOWN


func idle_animation() -> void:
	match facing:
		Facing.UP:
			animationPlayer.play("IdleUp")
		Facing.RIGHT:
			animationPlayer.play("IdleRight")
		Facing.DOWN:
			animationPlayer.play("IdleDown")
		Facing.LEFT:
			animationPlayer.play("IdleLeft")


func update_movement_input() -> Vector2:

	var last_pressed: Vector2 = Vector2.ZERO

	if Input.is_action_just_pressed("left"):
		last_pressed_time_left = Time.get_ticks_usec()
	if Input.is_action_just_pressed("up"):
		last_pressed_time_up = Time.get_ticks_usec()
	if Input.is_action_just_pressed("right"):
		last_pressed_time_right = Time.get_ticks_usec()
	if Input.is_action_just_pressed("down"):
		last_pressed_time_down = Time.get_ticks_usec()

	if (last_pressed_time_left > last_pressed_time_up
			and last_pressed_time_left > last_pressed_time_right
			and last_pressed_time_left > last_pressed_time_down):
		last_pressed = Vector2.LEFT
	if (last_pressed_time_up > last_pressed_time_left
			and last_pressed_time_up > last_pressed_time_right
			and last_pressed_time_up > last_pressed_time_down):
		last_pressed = Vector2.UP
	if (last_pressed_time_right > last_pressed_time_up
			and last_pressed_time_right > last_pressed_time_left
			and last_pressed_time_right > last_pressed_time_down):
		last_pressed = Vector2.RIGHT
	if (last_pressed_time_down > last_pressed_time_up
			and last_pressed_time_down > last_pressed_time_right
			and last_pressed_time_down > last_pressed_time_left):
		last_pressed = Vector2.DOWN

	if (Input.is_action_pressed("left")
			and (
				only_left_direction_pressed() or last_pressed == Vector2.LEFT
			)
	):
		return Vector2.LEFT

	elif (Input.is_action_pressed("right")
			and (
				only_right_direction_pressed() or last_pressed == Vector2.RIGHT
			)
	):
		return Vector2.RIGHT

	elif (Input.is_action_pressed("down")
			and (
				only_down_direction_pressed() or last_pressed == Vector2.DOWN
			)
	):
		return Vector2.DOWN

	elif (Input.is_action_pressed("up")
			and (
				only_up_direction_pressed() or last_pressed == Vector2.UP
			)
	):
		return Vector2.UP

	else:
		return Vector2.ZERO


func only_left_direction_pressed() -> bool:
	return (!Input.is_action_pressed("right")
		and !Input.is_action_pressed("down")
		and !Input.is_action_pressed("up"))


func only_up_direction_pressed() -> bool:
	return (!Input.is_action_pressed("right")
		and !Input.is_action_pressed("down")
		and !Input.is_action_pressed("left"))


func only_right_direction_pressed() -> bool:
	return (!Input.is_action_pressed("left")
		and !Input.is_action_pressed("down")
		and !Input.is_action_pressed("up"))


func only_down_direction_pressed() -> bool:
	return (!Input.is_action_pressed("right")
		and !Input.is_action_pressed("left")
		and !Input.is_action_pressed("up"))


func add_item(item: ItemData) -> void:
	PlayerData.items[item.get_name()] = item


func equip_item(item: Item) -> void:
	if item is Weapon:
		PlayerData.equip_item(item)


func _on_main_menu_item_activated(item_name: String) -> void:
	var item: ItemData = PlayerData.items[item_name]
	if item.item_type == ItemData.ItemType.WEAPON:
		dialogue.show_message("Weapon " + item.get_name(), main_menu)
		# TODO: Equip here for now to test equip function
	else:
		dialogue.show_message(item.get_name(), main_menu)
