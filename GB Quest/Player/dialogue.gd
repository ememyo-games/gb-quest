class_name Dialogue
extends PanelContainer


@onready var label: Label = $Label
@onready var next_button: Button = $Label/NextButton

var _message_queue: Array[String]
var _return_focus_to: PanelContainer


func show_message(message: String, return_focus_to: Control = null) -> void:
	_return_focus_to = return_focus_to
	_message_queue.push_back(message)
	show()
	show_next_message()


func show_next_message() -> void:
	label.text = _message_queue.pop_front()
	next_button.grab_focus()


func _on_next_button_button_down() -> void:
	if _message_queue.is_empty():
		print("hiding dialogue")
		hide()
		if _return_focus_to == null:
			get_tree().paused = false
		else:
			print(typeof(_return_focus_to))
			_return_focus_to.return_focus()
	else:
		show_next_message()
