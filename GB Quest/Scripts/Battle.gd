# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

extends Node2D

enum BattleState {
	PLAYER_CHOICE,
	PLAYER_ATTACKED,
	ENEMY_ATTACKED,
	ENEMY_TURN,
	ANIMATION,
	VICTORY,
	EXIT,
	GAME_OVER,
}

@export var battle_player: BattleCharacter
@export var battle_enemy: BattleCharacter
@export var player_health_bar: ProgressBar
@export var enemy_health_bar: ProgressBar
@export var battle_output: Label
@export var black_fade: BlackFade


var battle_state: BattleState = BattleState.PLAYER_CHOICE
var player_weapon_texture: Texture2D = preload(
	"res://Sprites/ShovelAttackAnimation.png"
)
var _export_vars_valid: bool = false

# Player setup
@onready var player: CharacterStats = PlayerData._stats
@onready var player_health: Label = %PlayerHealth
@onready var player_sprite: Sprite2D = battle_player.character_sprite
@onready var player_WeaponSprite: Sprite2D =  battle_player.weapon_sprite

# Enemy setup
@onready var enemy: CharacterStats = Global.battle_enemy
@onready var enemy_health: Label = %EnemyHealth
@onready var enemy_sprite: Sprite2D = battle_enemy.character_sprite
@onready var enemy_weapon_sprite: Sprite2D =  battle_enemy.weapon_sprite

# UI Setup
@onready var buttons_grid_container: GridContainer = %ButtonsGridContainer
@onready var attack_button: Button = %AttackButton
@onready var flee_button: Button = %FleeButton
@onready var items_button: Button = %ItemsButton
@onready var next_button: Button = %NextButton
@onready var spacer_panel: Panel = %SpacerPanel


func _ready() -> void:
	# For testing Battle scene directly
	if enemy == null:
		enemy = CharacterStats.new();
		enemy.set_stats("Debug Enemy", "Debug Enemy", 3, 3, 3, 15)
		Global.battle_enemy = enemy

	battle_player.set_chacter_stats(player)
	battle_enemy.set_chacter_stats(enemy)

	# Check @export vars are set
	if (battle_player == null
		or battle_enemy == null
		or player_health_bar == null
		or enemy_health_bar == null
		or battle_output == null
		or black_fade == null
	):
		push_error("At least one export variabel is null in Battle.gd")
		return
	else:
		_export_vars_valid = true

	player_health_bar.max_value = player.get_max_health()
	player_health_bar.value = player.get_current_health()
	player.print_stats()

	enemy_health_bar.max_value = enemy.get_max_health()
	enemy.print_stats()

	_update_health_bars_text()

	if PlayerData._equipped_weapon != null:
		#TODO Why does this not work??
		player_WeaponSprite.texture = PlayerData._equipped_weapon.sprite.texture
	else:
		player_WeaponSprite.texture = null

	enemy_weapon_sprite.texture = null
	battle_output.text = "Battle with " + enemy.character_name + "!"

	attack_button.grab_focus()

	await black_fade.fade_out()


func _on_AttackButton_pressed() -> void:
	battle_input("attack")


func battle_input(input: String) -> void:

	if battle_state == BattleState.ANIMATION:
		return
	# Empty battle output text string
	battle_output.text = ""

	if input == "attack" and battle_state == BattleState.PLAYER_CHOICE:
		_attack(battle_player, battle_enemy)
	elif input == "next" and battle_state == BattleState.ENEMY_TURN:
		_attack(battle_enemy, battle_player)
	elif input == "next" and battle_state == BattleState.VICTORY:
		exit_battle()
	elif input == "next" and battle_state == BattleState.GAME_OVER:
		game_over()
	elif input == "next" and battle_state == BattleState.EXIT:
		await black_fade.fade_in()
		Global.goto_scene("res://GameModes/Overworld.tscn")
	elif input == "item" and battle_state == BattleState.PLAYER_CHOICE:
		print("open item menu")

	_update_health_bars_text()


func player_attacked() -> void:

	# Calculate result
	var hit: bool = _resolve_attack(player, enemy)

	flee_button.disabled = true
	attack_button.disabled = true
	spacer_panel.show()
	next_button.show()
	# Animate result
	battle_state = BattleState.ANIMATION
	if (hit):
		await battle_enemy.do_hit_animation()
	else:
		await battle_enemy.do_dodge_animation()
	turn_complete()
	if battle_state != BattleState.VICTORY:
		battle_state = BattleState.ENEMY_TURN
		flee_button.disabled = false

	attack_button.disabled = false
	spacer_panel.hide()
	next_button.show()
	next_button.grab_focus()


func enemy_attacked() -> void:

	attack_button.disabled = true
	buttons_grid_container.hide()
	spacer_panel.show()
	# Calculate result
	var hit: bool = _resolve_attack(enemy, player)

	# Animate result
	battle_state = BattleState.ANIMATION
	if (hit):
		await battle_player.do_hit_animation()
	else:
		await battle_player.do_dodge_animation()

	attack_button.disabled = false
	#buttons_grid_container.show()
	#attack_button.grab_focus()
	turn_complete()
	if battle_state != BattleState.GAME_OVER:
		round_complete()


func _attack(attacker: BattleCharacter, defender: BattleCharacter) -> void:

	buttons_grid_container.hide()
	next_button.hide()
	spacer_panel.show()

	# Update output text string
	battle_output.text = (
		attacker.get_character_stats().character_name
		+ " attacks "
		+ defender.get_character_stats().character_name
	)
	# Show animation
	battle_state = BattleState.ANIMATION
	await attacker.do_attack_animation()

	# Update touch button to "Next"
	buttons_grid_container.hide()
	#spacer_panel.hide()
	#next_button.show()
	#next_button.grab_focus()
	if attacker.character_alignment == BattleCharacter.CharacterAlignment.PLAYER:
		await get_tree().create_timer(Global.user_settings["text_speed"]).timeout
		player_attacked()
		battle_state = BattleState.PLAYER_ATTACKED
	elif attacker.character_alignment == BattleCharacter.CharacterAlignment.ENEMY:
		await get_tree().create_timer(Global.user_settings["text_speed"]).timeout
		enemy_attacked()
		battle_state = BattleState.ENEMY_ATTACKED
	_update_health_bars_text()

	next_button.hide()


func _update_health_bars_text() -> void:
	if _export_vars_valid:
		player_health.text = player.character_name + "\nHP: " + str(player.get_current_health())
		enemy_health.text = enemy.character_name + "\nHP: " + str(enemy.get_current_health())


func turn_complete() -> void:
	if enemy.get_current_health() == 0:
		# Animate enemy death

		# Animate player celebration

		# Exit battle with win
		win_battle()

	if player.get_current_health() == 0:

		# Animate player death

		# Go to Game Over scene
		exit_battle()


func round_complete() -> void:
	# Update touch button to "Attack"
	spacer_panel.hide()
	next_button.hide()
	buttons_grid_container.show()
	attack_button.grab_focus()
	flee_button.disabled = false
	battle_state = BattleState.PLAYER_CHOICE


## Returs whether the attack hits or not
func _resolve_attack(attacker: CharacterStats, defender: CharacterStats) -> bool:

	var hit: bool = (
		(attacker.get_attack() + Dice.d6(2))
		- (defender.get_defense() + Dice.d6(1))  > 0
	)

	if hit:
		var damage: int = (
			(attacker.get_attack() + Dice.d6(2))
			- (defender.get_defense() + Dice.d6(1))
		)

		if damage < 0:
			damage = 0

		if damage > 0:
			var actual_damage: int = defender.take_damage(damage)
			battle_output.text += (
				"\n"
				+ defender.character_name
				+ " takes "
				+ str(actual_damage)
				+ " damage!"
			)
			return true
		else:
			battle_output.text += "\n" + defender.character_name + " blocked!"
			return false
	else:
		battle_output.text += "\n" + defender.character_name + " dodged!"
		return false


func flee() -> void:

	await black_fade.fade_in()
	PlayerData.pre_battle_position = PlayerData.pre_battle_position + Vector2(5, 5)

	Global.goto_scene("res://GameModes/Overworld.tscn")


func win_battle() -> void:
	print("Number of alive enemies remaining:", Global.alive_enemy_positions.size())
	var erased: bool = Global.alive_enemy_positions.erase(enemy.NodeName)
	if !erased:
		printerr("Tried to remove ",
			enemy.NodeName,
			"from alive_enemy_positions but it did not exist in the Dictionaty")
	Global.defeated_enemies.append(enemy.NodeName)
	Global.print_alive_enemies()

	battle_state = BattleState.VICTORY


func exit_battle() -> void:
	if battle_state == BattleState.VICTORY:
		battle_output.text = (
				"You defeated " + enemy.character_name + "!"
			)
		battle_state = BattleState.EXIT
	else:
		battle_output.text = (
				"You died."
			)
		flee_button.disabled = true

		# Update touch button to "Next"
		buttons_grid_container.hide()
		spacer_panel.hide()
		next_button.show()
		next_button.grab_focus()
		battle_state = BattleState.GAME_OVER


func game_over() -> void:
	await black_fade.fade_in()
	Global.goto_scene("res://GameModes/GameOver.tscn")


func _on_FleeButton_pressed() -> void:
	flee()


func _on_items_button_pressed() -> void:
	print("Open items menu")
	battle_input("item")
	pass # Replace with function body.


func _on_next_button_pressed() -> void:
	battle_input("next")
