# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

class_name Door
extends Area2D

@export var destination: Vector2 = Vector2.ZERO
@export var interscene_door: bool = false
@export var destination_scene: String = "res://"
@onready var color_rect: ColorRect = $ColorRect
@onready var animation_player: AnimationPlayer = $ColorRect/AnimationPlayer

func _on_StaticBody2D_body_entered(body: Node2D) -> void:

	if body is Player:
		await body.fade_out()

		if interscene_door and destination_scene != null:
			Global.goto_scene(destination_scene)
		else:
			body.set_position(Vector2(destination))

		await body.fade_in()
	else:

		if interscene_door and destination_scene != null:
			Global.goto_scene(destination_scene)
		else:
			body.set_position(Vector2(destination))
