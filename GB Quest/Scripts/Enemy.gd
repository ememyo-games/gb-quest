# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

class_name Enemy
extends CharacterBody2D

const SPEED: int = 15 # Should match FPS for smooth new_direction.
const IDLE_LIMIT: int = 64
const OVERLAP_LIMIT: int = 200
const STRAIGHT_MOVE_LIMIT: int = 255

enum {
	UP,
	RIGHT,
	DOWN,
	LEFT
}

@onready var animationPlayer: AnimationPlayer = $AnimationPlayer
var facing: int = DOWN
var movement_vector: Vector2 = Vector2.ZERO
var change_direction: bool = false
var rng: RandomNumberGenerator = RandomNumberGenerator.new()
var idle_count: int = 0
var overlapping_count: int = 0
var stuck: bool = true
var straight_movement_count: int = 0
var collision: KinematicCollision2D
var enemy_stats: CharacterStats
var reversed: bool = false

func _ready() -> void:
	rng.randomize()
	enemy_stats = CharacterStats.new()
	enemy_stats.set_stats(self.name, "Enemy", 1, 1, 1, 10)
	if name.begins_with("Monster"):
		enemy_stats.set_stats(self.name, "Monster", 2, 1, 3, 10)
	if enemy_stats.character_name == "New Character":
		enemy_stats.character_name = self.name
	movement_vector = new_direction()

func _physics_process(delta: float) -> void:
	if (movement_vector != Vector2.ZERO):
		animation_selection()
		straight_movement_count += 1
		if straight_movement_count > STRAIGHT_MOVE_LIMIT:
			movement_vector = new_direction()
			straight_movement_count = 0
	else:
		idle_animation()
		idle_count += 1
		if idle_count > IDLE_LIMIT:
			movement_vector = new_direction()
			idle_count = 0

	collision = move_and_collide(movement_vector * SPEED * delta)

	if collision:
		if reversed:
			movement_vector = new_direction()
			reversed = false
		else:
			movement_vector = collision.get_normal()
			reversed = true

		var collider: Node = collision.get_collider()
		if (collider.is_in_group("Player")):
			start_battle()

func animation_selection() -> void:
	if movement_vector.x > 0:
		animationPlayer.play("EnemyRunRight")
		facing = RIGHT
	elif movement_vector.x < 0:
		animationPlayer.play("EnemyRunLeft")
		facing = LEFT
	elif movement_vector.y < 0:
		animationPlayer.play("EnemyRunUp")
		facing = UP
	elif movement_vector.y > 0:
		animationPlayer.play("EnemyRunDown")
		facing = DOWN

func idle_animation() -> void:
	match facing:
		UP:
			animationPlayer.play("PlayerIdleUp")
		RIGHT:
			animationPlayer.play("PlayerIdleRight")
		DOWN:
			animationPlayer.play("PlayerIdleDown")
		LEFT:
			animationPlayer.play("PlayerIdleLeft")

func new_direction() -> Vector2:
	var random_number: int = rng.randi_range(-1, 1)
	var move_direction: Vector2
	match facing:
		UP:
			move_direction = Vector2(random_number, 0)
		RIGHT:
			move_direction = Vector2(0, random_number)
		DOWN:
			move_direction = Vector2(random_number, 0)
		LEFT:
			move_direction = Vector2(0, random_number)
	return move_direction

func start_battle() -> void:
	Global.save_enemy_positions()
	print ("Start Battle")
	print(name, " battle")
	Global.battle_enemy = enemy_stats
	Global.goto_scene("res://GameModes/Battle.tscn")

func get_id() -> int:
	return enemy_stats.ID
