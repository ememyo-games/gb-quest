# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

extends Node

var user_settings := {}

var current_scene: Node = null
var player: Player = null
var alive_enemy_positions: Dictionary = {}
var overworld_items: Dictionary = {}
var defeated_enemies: Array = []
var root: Node = null
var battle_enemy: CharacterStats
var id: int = 0
var audio_muted: bool = false
var settings_config_file: ConfigFile


func set_audio_muted(toggled_on: bool) -> void:
	audio_muted = toggled_on
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), audio_muted)
	save_settings()


func save_settings() -> void:
	settings_config_file.set_value("settings", "audio_muted", audio_muted)
	settings_config_file.save("user://settings")


func load_settings() -> void:
# Load data from a file.
	var err = settings_config_file.load("user://settings")

	# If the file didn't load, ignore it.
	if err != OK:
		push_error("Could not load settings file")
		return

	# Iterate over all sections.
	for section in settings_config_file.get_sections():

		if section == "settings":
			set_audio_muted(settings_config_file.get_value(section, "audio_muted"))


func reset() -> void:
	defeated_enemies = []
	alive_enemy_positions = {}


func _ready() -> void:
	root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	user_settings["text_speed"] = 0.5
	settings_config_file = ConfigFile.new()
	load_settings()


func goto_scene(path: String) -> void:
	if current_scene.name == "Overworld":
		player = root.get_node("Overworld/WorldObjects/Player")
		print("player = ", player.name)
		PlayerData.pre_battle_position = player.transform.origin
		print("player.transform = ", player.transform.origin)
	call_deferred("_deferred_goto_scene", path)


func _deferred_goto_scene(path: String) -> void:
	# It is now safe to remove the current scene
	current_scene.free()

	# Load the new scene.
	var scene: PackedScene = ResourceLoader.load(path)

	# Instance the new scene.
	current_scene = scene.instantiate()

	# Add it to the active scene, as child of root.
	get_tree().get_root().add_child(current_scene)

	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)

func save_enemy_positions() -> void:
	for enemy: Enemy in current_scene.get_node("WorldObjects/Enemies").get_children():
		if alive_enemy_positions.has(enemy.name):
			alive_enemy_positions[enemy.name] = enemy.transform.origin
			print("Saving position of", enemy.name)

func print_alive_enemies() -> void:
	print("Alive enemies")
	for enemy: String in alive_enemy_positions: # No types for dictionarys yet
		print("\t",enemy)

func get_next_id() -> int:
	id += 1
	return id
