extends Node2D

@export var outside_door: Vector2 = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var door: Door = get_node("Door")
	door.destination = outside_door
