# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

class_name Overworld
extends Node2D

@onready var items: Node2D = %Items
@onready var enemies: Node2D = %Enemies
@onready var canvas_layer: CanvasLayer = $CanvasLayer


func _ready() -> void:

	# init Enemies in Global if first time Overworld scene has started.
	if Global.alive_enemy_positions.is_empty() and Global.defeated_enemies.is_empty():
		for enemy: Enemy in enemies.get_children():
			print("Enemy added to alive", enemy.name)
			Global.alive_enemy_positions[enemy.name] = enemy.transform.origin
	else:
		for enemy: Enemy in enemies.get_children():
			if Global.alive_enemy_positions.has(enemy.name):
				print("set ", enemy.name, "'s positions to ", enemy.transform.origin)
				enemy.transform.origin = Global.alive_enemy_positions[enemy.name]
			if Global.defeated_enemies.has(enemy.name):
				enemy.queue_free()

	if Global.alive_enemy_positions.is_empty():
		Global.goto_scene("res://GameModes/Complete.tscn")
		return
	# Setup Items

	# init Items in Global if first time Overworld secne has started.
	#
	# Long term this should be done by checking save data for the level. Thus
	# the save data will maintain the state of a level including such things as
	# enemies, these items, and door locks etc.
	for item: Item in items.get_children():
		if item.name in PlayerData.items:
			# TODO: Why is this being removed from PlayerData?? It is not.
			item.queue_free()
		else:
			print("Item added to overworld_items", item.name)
			Global.overworld_items[item.name] = item

	Global.player.do_fade_in()

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("toggle_sound"):
		Global.set_audio_muted(!Global.audio_muted)
