# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

extends Node

var _stats: CharacterStats
var pre_battle_position: Vector2
var items: Dictionary = {}
var _equipped_weapon: Weapon

func _ready() -> void:
	reset()

func reset() -> void:
	_stats = CharacterStats.new()
	_stats.set_stats("Player", "Player", 3, 1, 3, 15)
	pre_battle_position = Vector2.ZERO
	items.clear()

func equip_item(item: Item) -> void:

	match typeof(item):
		Weapon:
			_equipped_weapon = item
			_stats.set_attack_bonus(item.weapon_damage)


