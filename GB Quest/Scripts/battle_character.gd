# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

class_name BattleCharacter
extends Node2D

enum Facing {
	LEFT,
	RIGHT,
}

enum CharacterAlignment {
	NEUTRAL,
	PLAYER,
	ENEMY,
}

@export var character_alignment: CharacterAlignment
@export var relative_position: Node2D
@export var character_sprite: Sprite2D
@export var weapon_sprite: Sprite2D
@export var _animation_player: AnimationPlayer

var _enemy_texture: Texture2D = preload("res://Sprites/EnemySheetBattle.png")
var _player_texture: Texture2D = preload("res://Sprites/PlayerSheetBattle.png")
var _monster_texture: Texture2D = preload("res://Sprites/MonsterSheet.png")
var _character_stats: CharacterStats

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if (
		character_alignment == null
		or relative_position == null
		or character_sprite == null
		or weapon_sprite == null
		or _animation_player == null
	):
		push_error("At least one export var is not set on BattleCharacter")
		return



func set_chacter_stats(stats: CharacterStats):
	_character_stats = stats
	if character_alignment == CharacterAlignment.PLAYER:
		# Flip sprites to face right
		relative_position.transform.scaled_local(Vector2(1, 1))
		character_sprite.texture = _player_texture

	elif character_alignment == CharacterAlignment.ENEMY:
		# Flip sprites to face left
		relative_position.transform.scaled_local(Vector2(-1, 1))

		if _character_stats.NodeName.begins_with("Monster"):
			character_sprite.texture = _monster_texture
		else:
			character_sprite.texture = _enemy_texture


func do_attack_animation() -> void:
	_animation_player.play("Attack")
	await _animation_player.animation_finished


func do_hit_animation() -> void:
	_animation_player.play("Hit")
	await _animation_player.animation_finished


func do_dodge_animation() -> void:
	_animation_player.play("Dodge")
	await _animation_player.animation_finished

func get_character_stats() -> CharacterStats:
	return _character_stats
