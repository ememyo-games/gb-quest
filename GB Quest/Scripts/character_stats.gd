# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

class_name CharacterStats
extends RefCounted

@export var character_name: String = "New Character"

var NodeName: String = ""
var ID: int
var _base_attack: int = 1
var _attack_bonus: int = 0
var _speed: int = 1
var _base_defense: int = 1
var _defense_bonus: int = 0
var _current_health: int = 10
var _max_health: int = 10


func _init() -> void:
	ID = Global.get_next_id()
	print("CharacterStats init ID: ", ID)

## Returns actual damage taken accounting for damage greater than their current
## health.
func take_damage(damage: int) -> int:
	var actual_damage: int = damage
	print("damage ", damage)
	print("actual damage ", actual_damage)
	var temp: int = _current_health - damage
	print("temp ", temp)
	if temp < 0:
		actual_damage = damage + temp # as temp is negative at this stage
		print("actual damage ", actual_damage)
		_current_health = 0
	else:
		_current_health = temp
	return actual_damage


func heal_damage(healing: int) -> void:
	var temp: int = _current_health + healing
	if temp > _max_health:
		_current_health = _max_health
	else:
		_current_health = temp


func set_stats(
		node_name: String,
		name: String,
		attack: int,
		speed: int,
		defense: int,
		max_health: int
) -> void:
	NodeName = node_name
	character_name = name
	_base_attack = attack
	_speed = speed
	_base_defense = defense
	_max_health = max_health
	_current_health = max_health


func get_id() -> int:
	return ID


func print_stats() -> void:
	print("NodeName: ", NodeName,
		"\ncharacter_name: ", character_name,
		"\nbase attack: ", _base_attack,
		"\nattack bonus: ", _attack_bonus,
		"\nspeed: ", _speed,
		"\nbase defense: ", _base_defense,
		"\ndefense bonus: ", _defense_bonus,
		"\nMaxHealth: ", _max_health,
		"\ncurrent health: ", _current_health)


func get_attack() -> int:
	return _base_attack + _attack_bonus


func set_attack_bonus(attack_bonus: int) -> void:
	_attack_bonus = attack_bonus

func get_defense() -> int:
	return _base_defense + _defense_bonus


func get_current_health() -> int:
	return _current_health


func get_max_health() -> int:
	return _max_health


func get_speed() -> int:
	return _speed
