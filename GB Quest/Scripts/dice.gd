extends Node

var rng: RandomNumberGenerator = RandomNumberGenerator.new()

func _ready() -> void:
	rng.randomize()

func d6(quantity: int) -> int:
	var result: int = 0
	for i in quantity:
		result += rng.randi_range(1, 6)
	return result
