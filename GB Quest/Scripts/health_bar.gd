# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

class_name HealthBar
extends ProgressBar

@export var PlayerHealth: bool = false


func _process(_delta: float) -> void:
	if PlayerHealth:
		value = PlayerData._stats.get_current_health()
	else:
		value = Global.battle_enemy.get_current_health()
