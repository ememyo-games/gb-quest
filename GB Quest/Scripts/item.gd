# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

class_name Item
extends StaticBody2D

var _item_data: ItemData = ItemData.new():
	get:
		return _item_data

@export var _item_game_name: String:
	set(value):
		_item_game_name = value
		_item_data.set_name(value)

@export var _item_description: String:
	set(value):
		_item_data.set_description(value)
	get:
		return _item_data.get_description()

@export_enum("a", "the") var _determiner: String = "a":
	set(value):
		_item_data._determiner = value

@export_enum("Normal", "Weapon") var _item_type: String = "Normal":
	set(value):
		_item_data.item_type = ItemData.ItemType[value.to_upper()]

@export var sprite: Sprite2D

func _ready() -> void:
	if _item_game_name == "":
		_item_game_name = name
	_item_data._determiner = _determiner

func interact(player: Player) -> void:
	player.add_item(_item_data);
	queue_free()

func get_item_name() -> String:
	return _item_game_name

func get_determinative() -> String:
	return str(_determiner) + " " + _item_game_name
