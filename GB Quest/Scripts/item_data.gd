class_name ItemData
extends Object

enum ItemType {
	WEAPON,
	NORMAL,
}

var _description: String
var _name: String
var item_type: ItemType:
	get:
		return item_type
	set(value):
		item_type = value

var _determiner: String:
	get:
		return _determiner
	set(value):
		_determiner = value


func set_description(description: String) -> void:
	_description = description


func get_description() -> String:
	return _description


func set_name(name: String) -> void:
	_name = name


func get_name() -> String:
	return _name
