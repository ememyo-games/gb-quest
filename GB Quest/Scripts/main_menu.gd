# This file is part of GB Quest.
#
# GB Quest is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# GB Quest is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with GB Quest.  If not, see <https://www.gnu.org/licenses/>.

class_name MainMenu
extends PanelContainer

@onready var resume_button: Button = $MainMenuButtons/VBoxContainer/ResumeButton
@onready var main_menu_buttons: CenterContainer = $MainMenuButtons
@onready var settings_menu: CenterContainer = $SettingsMenu
@onready var settings_back_button: Button = $SettingsMenu/VBoxContainer/SettingsBackButton
@onready var mute_audio_check_box: CheckBox = $SettingsMenu/VBoxContainer/MuteAudioCheckBox
@onready var item_list: ItemList = $ItemMenu/VBoxContainer/ItemList
@onready var items_back_button: Button = $ItemMenu/VBoxContainer/ItemsBackButton
@onready var item_menu: CenterContainer = $ItemMenu

@export var sub_menus: Array[Control]

const SELECTED_ITEM: Texture2D = preload("res://Sprites/SelectedItem.png")

func _ready() -> void:
	main_menu_buttons.show()
	settings_menu.hide()
	resume_button.grab_focus()
	item_menu.hide()


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("menu"):
		if get_tree().paused:
			print("paused and menu pressed")
			close_menu()
		else:
			print("not paused and menu pressed")
			open_menu()


func open_menu() -> void:
	show()
	resume_button.grab_focus()
	get_tree().paused = true
	item_list.clear()

	for item: ItemData in PlayerData.items.values():
		item_list.add_item(item.get_name(), SELECTED_ITEM)


func close_menu() -> void:
	hide()
	get_tree().paused = false


func open_settings_menu() -> void:
	main_menu_buttons.hide()
	settings_menu.hide()
	settings_menu.show()
	mute_audio_check_box.grab_focus()
	mute_audio_check_box.set_pressed_no_signal(Global.audio_muted)


func _on_resume_button_button_up() -> void:
	close_menu()


func _on_quit_button_button_up() -> void:
	get_tree().quit(0)


func _on_settings_button_button_up() -> void:
	open_settings_menu()


func _on_settings_back_button_button_up() -> void:
	settings_menu.hide()
	main_menu_buttons.show()
	resume_button.grab_focus()


func _on_mute_audio_check_box_toggled(toggled_on: bool) -> void:
	Global.set_audio_muted(toggled_on)
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), Global.audio_muted)


func _on_items_button_button_up() -> void:
	main_menu_buttons.hide()
	settings_menu.hide()
	item_menu.show()
	if item_list.item_count > 0:
		item_list.grab_focus()
		item_list.select(0)
		show_arrow_on_selecte_item(0)
	else:
		items_back_button.grab_focus()


func _on_items_back_button_button_up() -> void:
	item_menu.hide()
	main_menu_buttons.show()
	resume_button.grab_focus()


func _on_item_list_item_selected(index: int) -> void:
	show_arrow_on_selecte_item(index)


func show_arrow_on_selecte_item(index: int) -> void:
	for i in range(0, item_list.item_count):
		# Disable all tooltips while we're iterating through
		item_list.set_item_tooltip_enabled(i, false)
		if i == index:
			item_list.set_item_icon_modulate(i, Color(0,0,0,1))
		else:
			item_list.set_item_icon_modulate(i, Color(0,0,0,0))


func _on_item_list_focus_exited() -> void:
	for i in range(0, item_list.item_count):
		item_list.set_item_icon_modulate(i, Color(0,0,0,0))


func _on_item_list_focus_entered() -> void:
	item_list.select(0)
	show_arrow_on_selecte_item(0)


signal item_activated(item_name: String)


func _on_item_list_item_activated(index: int) -> void:
	item_activated.emit(item_list.get_item_text(index))
	print("item " + item_list.get_item_text(index) + " activated")


func return_focus() -> void:
	for sub_menu in sub_menus:
		if sub_menu.visible:
			sub_menu.get_child(0).get_child(0).grab_focus()
			print("grabbing focus of " + sub_menu.name)
