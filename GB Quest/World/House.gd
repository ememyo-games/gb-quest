extends Node2D

@export var destination: Vector2 = Vector2.ZERO
@onready var door: Door = $Door


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	door.destination = destination

