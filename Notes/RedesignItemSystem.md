
Redesign Item System
====================

Item system was quickly hacked together. Needs to be properly designed.


Current Item System
-------------------

### Global.gd

    enum ItemOwner {OVERWORLD, PLAYER}

Shouldn't this be a property of an Item object? It is... Should it be an ENUM?

**DONE**

    enum ItemType {WEAPON, CONSUMABLE}

Shouldn't this be a property of an Item object? It is... Should it be an ENUM?

**DONE**

    var overworld_items: Dictionary = {}

Seems okay to maintain some record of items that exist in the overworld.

### Item.gd

Item class. this a is good start.

### consumable_item.gd

Item sub-class

### Overworld.gd

items Node2D as parent of items in the overworld

_ready() goes through items node child nodes and adds them to the Global.player
if they are in PlayerData.items

### Battle.gd

Checks if PlayerData.items contains key "Shovel"

### PlayerData.gb

has a dictionary of items

### Player.gd

interact() checks sub-type of Node2Ds interacted with. If Item then acts 
accordingly.


New Item System
---------------

Move ENUMs ItemOwner and ItemType to Item class. **DONE**

Remove ItemOwner. Have the owner keep a reference instead.

### item.gd

interact() should not need to update item_owner. item_owner should be removed.
Owners should keep track of what items they own instead. **DONE**

### Overworld.gd

Instead of moving item nodes to the player, just destroy them and keep track of 
what the player has in data. Used to destroy them, why does that not work?
**DONE**

### Battle.gd

Should instead check if PlayerData.items contains an item class object of
sub-type weapon, and get the required texture from the item_data

Requires adding texture to Item class

**IN PROGRESS**

### Player.gb

add_item() should not be messing around with the node. The item data should be
recorded and the node destroyed.
Also the weapon should not be auto-equipped. Equipping should be done in a menu.

Why is _on_main_menu_item_activated() here? Could it be in a menu script?

### PlayerData.gd

Should equip_weapon() take a Weapon, not an Item? Meaning we would need a Weapon
subclass
or should it be equip_item() and do the "right thing" depending on the item's
sub-type?
