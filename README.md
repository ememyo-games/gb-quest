GB Quest
========

An 8-bit style RPG game reminiscent of pocket console games of the late 1900s.

This game is currently in a pre-alpha prototype state.

Licence
-------

The code in this project is made available under the AGPL v3.0 free 
software licence.

The other assets (images, audio, textures, sprites, music, sound effects etc.)
are made available under the CC-BY-SA 4.0 licence.

This project uses the pokemon-font v1.8.2 which is Copyright (c) 2016-2017, Superpencil (https://superpencil.com | support@superpencil.com>) and licensed under the SIL Open Font License, Version 1.1.

Tech
----

The game is made using version 4 of the free software Godot game engine.

Instructions below are for exporting the game to supported platforms (currently
GNU/Linux, and Android).

### Android Export Setup

1.	Install OpenJDK 11

2.	Install latest AndroidSDK

	1.  Download from 
		https://dl.google.com/android/repository/commandlinetools-linux-9477386_latest.zip

	2.  Unzip and copy `cmdline-tools` directory to somewhere you like, e.g
		`~/bin/`

	3.  Add the cmdline-tools/bin directory location to you `$PATH`

	5.  Decide where you want the Android SDK to be installed, e.g
		`~/AndroidSDK/`	and replace `<android_sdk_path>` with this location in
		the command in the following step.

	4.  Run `sdkmanager --sdk_root=<android_sdk_path> "platform-tools" \
		"build-tools;30.0.3" "platforms;android-31" "cmdline-tools;latest" \
		"cmake;3.10.2.4988404" "ndk;21.4.7075529"`

### Experimental Mac OS Export notes

MacOS export requires the user to apply the below via Terminal:

    chmod +x "GB\ Quest.app/Contents/MacOS/GB Quest"

